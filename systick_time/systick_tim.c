/**
 * filename : systick_tim.c
 *
 * created  : 2018/09/05
 *
 * Hyeon-ki, Hong
 * hhk7734@gmail.com
 *
 * purpose : millis, micros, delay
 */

#include "systick_time.h"

extern HAL_TickFreqTypeDef uwTickFreq;

void HAL_IncTick( void )
{
    uwTick += uwTickFreq;
}

void HAL_Delay( uint32_t Delay )
{
    uint32_t tickstart = HAL_GetTick();

    while ( ( HAL_GetTick() - tickstart ) < Delay )
    {
    }
}
