/**
 * filename : systick_time.h
 *
 * created  : 2018/09/05
 *
 * Hyeon-ki, Hong
 * hhk7734@gmail.com
 *
 * purpose : millis, micros, delay
 */

#ifndef _SYSTICK_TIME_H_
#define _SYSTICK_TIME_H_
#ifdef __cplusplus
extern "C"
{
#endif

/**
 * include header for stm32 mcu
 */
#include "stm32f1xx_hal.h"

/**
 * set system clock
 */
#define COTEX_TIMER_SYSTEM_DIV  1UL
#define SYSTEM_CORE_CLOCK       72000000UL
#define ASSEMBLY_CYCLE          6UL         // delay_us()

#define COUNT_PER_MICROSECOND   ( SYSTEM_CORE_CLOCK / 1000000UL / COTEX_TIMER_SYSTEM_DIV)
#define REPEAT_FOR_MICROSECOND  ( SYSTEM_CORE_CLOCK / 1000000UL / ASSEMBLY_CYCLE) // 72 / 6 == 12

extern __IO uint32_t uwTick;

static inline uint32_t millis( void )
{
    return uwTick;
}

static inline uint32_t micros( void )
{
    uint32_t ms;
    uint32_t count;

    do
    {
        ms = millis();
        count = SysTick->VAL;
        // for interrupt
        asm volatile("nop");
        asm volatile("nop");
    }
    while ( ms != millis() );

    return ( ( ms * 1000UL ) + ( SysTick->LOAD + 1 - count ) / COUNT_PER_MICROSECOND );
}

static inline void delay_ms( uint32_t Delay )
{
    HAL_Delay( Delay );
}

static inline void delay_us( uint32_t Delay )
{
    Delay *= REPEAT_FOR_MICROSECOND;

    --Delay; // r0 = us
    // label 1: r0 -= 1
    // branch. if high, go to label 1 backward
    // maybe 6 cycle
    asm volatile(
            "   mov r0, %[Delay]       \n\t"
            "1: subs r0, #1            \n\t"
            "   bhi 1b                 \n\t"
            :
            : [Delay] "r" (Delay)
            : "r0");
}

#ifdef __cplusplus
}
#endif

#endif /* _SYSTICK_TIME_H_ */
