/**
 * filename : mpu9250.c
 *
 * created  : 2018/09/09
 * edited   : 2018/09/11
 *
 * Hyeon-ki, Hong
 * hhk7734@gmail.com
 *
 * purpose : MPU-9250 ; accelerometer, gyroscope, magnetometer
 */

#include "mpu9250.h"
#include "systick_time.h"
#include <math.h>

#if MPU9250_AD0
#define MPU9250_ADDRESS 0x69
#else
#define MPU9250_ADDRESS 0x68
#endif

#define AK8963_ADDRESS  0x0C
#define AK8963_WHO_AM_I 0x00
#define AK8963_INFO     0x01
#define AK8963_ST1      0x02
#define AK8963_XOUT_L   0x03
#define AK8963_XOUT_H   0x04
#define AK8963_YOUT_L   0x05
#define AK8963_YOUT_H   0x06
#define AK8963_ZOUT_L   0x07
#define AK8963_ZOUT_H   0x08
#define AK8963_ST2      0x09
#define AK8963_CNTL1    0x0A
#define AK8963_CNTL2    0x0B
#define AK8963_ASTC     0x0C
#define AK8963_I2CDIS   0x0F
#define AK8963_ASAX     0x10
#define AK8963_ASAY     0x11
#define AK8963_ASAZ     0x12

#define MPU9250_SELF_TEST_X_GYRO    0x00
#define MPU9250_SELF_TEST_Y_GYRO    0x01
#define MPU9250_SELF_TEST_Z_GYRO    0x02
#define MPU9250_SELF_TEST_X_ACCEL   0x0D
#define MPU9250_SELF_TEST_Y_ACCEL   0x0E
#define MPU9250_SELF_TEST_Z_ACCEL   0x0F
#define MPU9250_XG_OFFSET_H         0x13
#define MPU9250_XG_OFFSET_L         0x14
#define MPU9250_YG_OFFSET_H         0x15
#define MPU9250_YG_OFFSET_L         0x16
#define MPU9250_ZG_OFFSET_H         0x17
#define MPU9250_ZG_OFFSET_L         0x18
#define MPU9250_SMPLRT_DIV          0x19
#define MPU9250_CONFIG              0x1A
#define MPU9250_GYRO_CONFIG         0x1B
#define MPU9250_ACCEL_CONFIG        0x1C
#define MPU9250_ACCEL_CONFIG2       0x1D
#define MPU9250_LP_ACCEL_ODR        0x1E
#define MPU9250_WOM_THR             0x1F
#define MPU9250_FIFO_EN             0x23
#define MPU9250_I2C_MST_CTRL        0x24
#define MPU9250_I2C_SLV0_ADDR       0x25
#define MPU9250_I2C_SLV0_REG        0x26
#define MPU9250_I2C_SLV0_CTRL       0x27
#define MPU9250_I2C_SLV1_ADDR       0x28
#define MPU9250_I2C_SLV1_REG        0x29
#define MPU9250_I2C_SLV1_CTRL       0x2A
#define MPU9250_I2C_SLV2_ADDR       0x2B
#define MPU9250_I2C_SLV2_REG        0x2C
#define MPU9250_I2C_SLV2_CTRL       0x2D
#define MPU9250_I2C_SLV3_ADDR       0x2E
#define MPU9250_I2C_SLV3_REG        0x2F
#define MPU9250_I2C_SLV3_CTRL       0x30
#define MPU9250_I2C_SLV4_ADDR       0x31
#define MPU9250_I2C_SLV4_REG        0x32
#define MPU9250_I2C_SLV4_DO         0x33
#define MPU9250_I2C_SLV4_CTRL       0x34
#define MPU9250_I2C_SLV4_DI         0x35
#define MPU9250_I2C_MST_STATUS      0x36
#define MPU9250_INT_PIN_CFG         0x37
#define MPU9250_INT_ENABLE          0x38
#define MPU9250_INT_STATUS          0x3A
#define MPU9250_ACCEL_XOUT_H        0x3B
#define MPU9250_ACCEL_XOUT_L        0x3C
#define MPU9250_ACCEL_YOUT_H        0x3D
#define MPU9250_ACCEL_YOUT_L        0x3E
#define MPU9250_ACCEL_ZOUT_H        0x3F
#define MPU9250_ACCEL_ZOUT_L        0x40
#define MPU9250_TEMP_OUT_H          0x41
#define MPU9250_TEMP_OUT_L          0x42
#define MPU9250_GYRO_XOUT_H         0x43
#define MPU9250_GYRO_XOUT_L         0x44
#define MPU9250_GYRO_YOUT_H         0x45
#define MPU9250_GYRO_YOUT_L         0x46
#define MPU9250_GYRO_ZOUT_H         0x47
#define MPU9250_GYRO_ZOUT_L         0x48
#define MPU9250_EXT_SENS_DATA_00    0x49
#define MPU9250_EXT_SENS_DATA_01    0x4A
#define MPU9250_EXT_SENS_DATA_02    0x4B
#define MPU9250_EXT_SENS_DATA_03    0x4C
#define MPU9250_EXT_SENS_DATA_04    0x4D
#define MPU9250_EXT_SENS_DATA_05    0x4E
#define MPU9250_EXT_SENS_DATA_06    0x4F
#define MPU9250_EXT_SENS_DATA_07    0x50
#define MPU9250_EXT_SENS_DATA_08    0x51
#define MPU9250_EXT_SENS_DATA_09    0x52
#define MPU9250_EXT_SENS_DATA_10    0x53
#define MPU9250_EXT_SENS_DATA_11    0x54
#define MPU9250_EXT_SENS_DATA_12    0x55
#define MPU9250_EXT_SENS_DATA_13    0x56
#define MPU9250_EXT_SENS_DATA_14    0x57
#define MPU9250_EXT_SENS_DATA_15    0x58
#define MPU9250_EXT_SENS_DATA_16    0x59
#define MPU9250_EXT_SENS_DATA_17    0x5A
#define MPU9250_EXT_SENS_DATA_18    0x5B
#define MPU9250_EXT_SENS_DATA_19    0x5C
#define MPU9250_EXT_SENS_DATA_20    0x5D
#define MPU9250_EXT_SENS_DATA_21    0x5E
#define MPU9250_EXT_SENS_DATA_22    0x5F
#define MPU9250_EXT_SENS_DATA_23    0x60
#define MPU9250_I2C_SLV0_DO         0x63
#define MPU9250_I2C_SLV1_DO         0x64
#define MPU9250_I2C_SLV2_DO         0x65
#define MPU9250_I2C_SLV3_DO         0x66
#define MPU9250_I2C_MST_DELAY_CTRL  0x67
#define MPU9250_SIGNAL_PATH_RESET   0x68
#define MPU9250_MOT_DETECT_CTRL     0x69
#define MPU9250_USER_CTRL           0x6A
#define MPU9250_PWR_MGMT_1          0x6B
#define MPU9250_PWR_MGMT_2          0x6C
#define MPU9250_FIFO_COUNTH         0x72
#define MPU9250_FIFO_COUNTL         0x73
#define MPU9250_FIFO_R_W            0x74
#define MPU9250_WHO_AM_I_MPU9250    0x75
#define MPU9250_XA_OFFSET_H         0x77
#define MPU9250_XA_OFFSET_L         0x78
#define MPU9250_YA_OFFSET_H         0x7A
#define MPU9250_YA_OFFSET_L         0x7B
#define MPU9250_ZA_OFFSET_H         0x7D
#define MPU9250_ZA_OFFSET_L         0x7E

I2C_HandleTypeDef *mpu9250;

int16_t mpu9250_acc_offset[3] = { 0 };
int16_t mpu9250_gyro_offset[3] = { 0 };
int16_t mpu9250_mag_offset[3] = { 0 };
int16_t mpu9250_mag_sensitivity[3] = { 32 };

HAL_StatusTypeDef mpu9250_write_reg( uint8_t reg , uint8_t data )
{
    return HAL_I2C_Mem_Write( mpu9250 , MPU9250_ADDRESS << 1 , reg , 1 , & data , 1 , 1 );
}

HAL_StatusTypeDef mpu9250_read_reg( uint8_t reg , uint8_t *data , uint8_t size )
{
    return HAL_I2C_Mem_Read( mpu9250 , MPU9250_ADDRESS << 1 , reg , 1 , data , size , size );
}

HAL_StatusTypeDef mpu9250_read_3_axis( uint8_t reg , int16_t *xyz )
{
    uint8_t temp[6];
    HAL_StatusTypeDef result = mpu9250_read_reg( reg , temp , 6 );
    xyz[0] = ( temp[0] << 8 ) | temp[1];
    xyz[1] = ( temp[2] << 8 ) | temp[3];
    xyz[2] = ( temp[4] << 8 ) | temp[5];
    return result;
}

HAL_StatusTypeDef mpu9250_setup( I2C_HandleTypeDef *i2c )
{
    mpu9250 = i2c;

    // reset all register
    HAL_StatusTypeDef result = mpu9250_write_reg( MPU9250_PWR_MGMT_1 , 0b10000000 );
    delay_ms( 100 );
    mpu9250_write_reg( MPU9250_PWR_MGMT_1 , 0b00000001 );// auto selects the clock source

    mpu9250_write_reg( MPU9250_CONFIG , MPU9250_DLPF_CFG );
    mpu9250_write_reg( MPU9250_GYRO_CONFIG , MPU9250_GYRO_FS_SEL | MPU9250_GYRO_FCHOICE );
    mpu9250_write_reg( MPU9250_ACCEL_CONFIG , MPU9250_ACCEL_FS_SEL );
    mpu9250_write_reg( MPU9250_ACCEL_CONFIG2 , MPU9250_ACCEL_FCHOICE | MPU9250_A_DLPFCFG );

    mpu9250_write_reg( MPU9250_I2C_MST_CTRL , 0b00001101 ); // i2c master clock 400kHz
    mpu9250_write_reg( MPU9250_I2C_SLV0_ADDR , 0x80 | AK8963_ADDRESS ); // read AK8963
    mpu9250_write_reg( MPU9250_I2C_SLV0_REG , AK8963_YOUT_L );// i2c_slv0 enable, swap byte, group of 2 bytes, read 2 bytes
    mpu9250_write_reg( MPU9250_I2C_SLV0_CTRL , 0b11010010 );
    mpu9250_write_reg( MPU9250_I2C_SLV1_ADDR , 0x80 | AK8963_ADDRESS );
    mpu9250_write_reg( MPU9250_I2C_SLV1_REG , AK8963_XOUT_L );
    mpu9250_write_reg( MPU9250_I2C_SLV1_CTRL , 0b11010010 );
    mpu9250_write_reg( MPU9250_I2C_SLV2_ADDR , 0x80 | AK8963_ADDRESS );
    /**
     * mag z-axis == acc (-)z-axis
     * acc xyz
     * mag xy(-z)
     */
    mpu9250_write_reg( MPU9250_I2C_SLV2_REG , AK8963_ZOUT_L );
    mpu9250_write_reg( MPU9250_I2C_SLV2_CTRL , 0b11010011 );// read 3 bytes; when ST2 register is read, AK8963 judges that data reading is finished
    mpu9250_write_reg( MPU9250_I2C_SLV4_ADDR , AK8963_ADDRESS ); // write AK8963
    mpu9250_write_reg( MPU9250_I2C_SLV4_REG , AK8963_CNTL1 );
    mpu9250_write_reg( MPU9250_I2C_SLV4_DO , 0b00010110 ); // mag output 16bit, 100Hz rate
    mpu9250_write_reg( MPU9250_I2C_SLV4_CTRL , 0b10000000 ); // i2c_slv4 enable, this bit auto clears
    mpu9250_write_reg( MPU9250_USER_CTRL , 0b00100000 ); // i2c master enable

    return result;
}

HAL_StatusTypeDef mpu9250_acc_calibration( void )
{
    int16_t raw_acc[3];
    int32_t raw_acc_sum[3] = { 0 };
    HAL_StatusTypeDef result;

    for ( int16_t i = 0 ; i < 256 ; ++i )
    {
        result = mpu9250_read_3_axis( MPU9250_ACCEL_XOUT_H , raw_acc );
        if ( result != HAL_OK )
        {
            return result;
        }
        raw_acc_sum[0] += raw_acc[0];
        raw_acc_sum[1] += raw_acc[1];
        raw_acc_sum[2] += raw_acc[2];
        delay_ms( 2 );
    }

    mpu9250_acc_offset[0] = raw_acc_sum[0] >> 8;
    mpu9250_acc_offset[1] = raw_acc_sum[1] >> 8;
    mpu9250_acc_offset[2] = raw_acc_sum[2] >> 8;

    // an axis must be parallel to vertical
    for ( int16_t i = 0 ; i < 3 ; ++i )
    {
        if ( mpu9250_acc_offset[i] > MPU9250_ACCEL_1G_SCALE * 0.9 )
        {
            mpu9250_acc_offset[i] -= MPU9250_ACCEL_1G_SCALE;
        }
        else if ( mpu9250_acc_offset[i] < - MPU9250_ACCEL_1G_SCALE * 0.9 )
        {
            mpu9250_acc_offset[i] += MPU9250_ACCEL_1G_SCALE;
        }
    }

    return result;
}

HAL_StatusTypeDef mpu9250_gyro_calibration( void )
{
    int16_t raw_gyro[3];
    int32_t raw_gyro_sum[3] = { 0 };
    HAL_StatusTypeDef result;

    for ( int16_t i = 0 ; i < 256 ; ++i )
    {
        result = mpu9250_read_3_axis( MPU9250_GYRO_XOUT_H , raw_gyro );
        if ( result != HAL_OK )
        {
            return result;
        }
        raw_gyro_sum[0] += raw_gyro[0];
        raw_gyro_sum[1] += raw_gyro[1];
        raw_gyro_sum[2] += raw_gyro[2];
        delay_ms( 2 );
    }

    mpu9250_gyro_offset[0] = raw_gyro_sum[0] >> 8;
    mpu9250_gyro_offset[1] = raw_gyro_sum[1] >> 8;
    mpu9250_gyro_offset[2] = raw_gyro_sum[2] >> 8;

    return result;
}

HAL_StatusTypeDef mpu9250_mag_calibration( void )
{
    HAL_StatusTypeDef result;
    uint8_t temp[6];
    double component[6];
    double sigma_A[21] = { 0 };
    double sigma_b[6] = { 0 };

// M^t * M * coef = M^t * 1[k:1]
// sA * coef = sb
// ax^2 + by^2 + cz^2 + dx + ey + fz = 1000
    for ( int16_t k = 0 ; k < 100 ; ++k )
    {
        result = mpu9250_read_reg( MPU9250_EXT_SENS_DATA_00 , temp , 6 );
        if ( result != HAL_OK )
        {
            return result;
        }
        component[3] = ( int16_t ) ( ( temp[0] << 8 ) | temp[1] );
        component[4] = ( int16_t ) ( ( temp[2] << 8 ) | temp[3] );
        component[5] = ( int16_t ) ( - ( ( temp[4] << 8 ) | temp[5] ) ); // mag z-axis is reversed
        component[0] = component[3] * component[3];
        component[1] = component[4] * component[4];
        component[2] = component[5] * component[5];

        // Lower triangular matrix
        for ( int16_t i = 0 ; i < 6 ; ++i )
        {
            temp[i] = i * ( i + 1 ) >> 1; // row
            for ( int16_t j = 0 ; j < i + 1 ; ++j )
            {
                sigma_A[temp[i] + j] += component[i] * component[j];
            }
            sigma_b[i] += component[i];
        }

        delay_ms( 200 );
    }

// Cholesky decomposition
// sA = L * L^t
    for ( int16_t i = 0 ; i < 6 ; ++i )
    {
        for ( int16_t j = 0 ; j < i + 1 ; ++j )
        {
            if ( i == j )
            {
                sigma_A[temp[i] + i] = sqrt( sigma_A[temp[i] + i] );
            }
            else
            {
                for ( int16_t k = 0 ; k < j ; ++k )
                {
                    sigma_A[temp[i] + j] -= sigma_A[temp[i] + k] * sigma_A[temp[j] + k];
                }
                sigma_A[temp[i] + j] /= sigma_A[temp[j] + j];
                sigma_A[temp[i] + i] -= sigma_A[temp[i] + j] * sigma_A[temp[i] + j];
            }
        }

        // R^2 == 1000
        sigma_b[i] *= 1000;
    }

// L * ( L^t * coef ) = b
// L * x = b
// component == x
    for ( int16_t i = 0 ; i < 6 ; ++i )
    {
        for ( int16_t j = 0 ; j < i ; ++j )
        {
            sigma_b[i] -= sigma_A[temp[i] + j] * component[j];
        }
        component[i] = sigma_b[i] / sigma_A[temp[i] + i];
    }

// L^t * coef = x
// component == x
// sigma_b == coef
    for ( int16_t i = 5 ; i >= 0 ; --i )
    {
        for ( int16_t j = 5 ; j > i ; --j )
        {
            component[i] -= sigma_A[temp[j] + i] * sigma_b[j];
        }
        sigma_b[i] = component[i] / sigma_A[temp[i] + i];
    }

    mpu9250_mag_offset[0] = ( int16_t ) ( - sigma_b[3] / sigma_b[0] ) >> 1;
    mpu9250_mag_offset[1] = ( int16_t ) ( - sigma_b[4] / sigma_b[1] ) >> 1;
    mpu9250_mag_offset[2] = ( int16_t ) ( - sigma_b[5] / sigma_b[2] ) >> 1;
    mpu9250_mag_sensitivity[0] = 32;
    mpu9250_mag_sensitivity[1] = 32 * sqrt( sigma_b[1] / sigma_b[0] );
    mpu9250_mag_sensitivity[2] = 32 * sqrt( sigma_b[2] / sigma_b[0] );

    return result;
}

HAL_StatusTypeDef mpu9250_get_acc( int16_t *xyz )
{
    HAL_StatusTypeDef result = mpu9250_read_3_axis( MPU9250_ACCEL_XOUT_H , xyz );
    xyz[0] -= mpu9250_acc_offset[0];
    xyz[1] -= mpu9250_acc_offset[1];
    xyz[2] -= mpu9250_acc_offset[2];
    return result;
}

HAL_StatusTypeDef mpu9250_get_gyro( int16_t *xyz )
{
    HAL_StatusTypeDef result = mpu9250_read_3_axis( MPU9250_GYRO_XOUT_H , xyz );
    xyz[0] -= mpu9250_gyro_offset[0];
    xyz[1] -= mpu9250_gyro_offset[1];
    xyz[2] -= mpu9250_gyro_offset[2];
    return result;
}

HAL_StatusTypeDef mpu9250_get_mag( int16_t *xyz )
{
    HAL_StatusTypeDef result = mpu9250_read_3_axis( MPU9250_EXT_SENS_DATA_00 , xyz );
    xyz[2] = - xyz[2]; // mag z-axis is reversed
    xyz[0] = ( ( int32_t ) ( xyz[0] - mpu9250_mag_offset[0] ) * mpu9250_mag_sensitivity[0] ) >> 5;
    xyz[1] = ( ( int32_t ) ( xyz[1] - mpu9250_mag_offset[1] ) * mpu9250_mag_sensitivity[1] ) >> 5;
    xyz[2] = ( ( int32_t ) ( xyz[2] - mpu9250_mag_offset[2] ) * mpu9250_mag_sensitivity[2] ) >> 5;
    return result;
}

HAL_StatusTypeDef mpu9250_get_all( int16_t *acc_xyz , int16_t *gyro_xyz , int16_t *mag_xyz )
{
    uint8_t temp[20];
    HAL_StatusTypeDef result = mpu9250_read_reg( MPU9250_ACCEL_XOUT_H , temp , 20 );

    acc_xyz[0] = ( temp[0] << 8 ) | temp[1];
    acc_xyz[1] = ( temp[2] << 8 ) | temp[3];
    acc_xyz[2] = ( temp[4] << 8 ) | temp[5];

    gyro_xyz[0] = ( temp[8] << 8 ) | temp[9];
    gyro_xyz[1] = ( temp[10] << 8 ) | temp[11];
    gyro_xyz[2] = ( temp[12] << 8 ) | temp[13];
    mag_xyz[0] = ( temp[14] << 8 ) | temp[15];
    mag_xyz[1] = ( temp[16] << 8 ) | temp[17];
    mag_xyz[2] = - ( ( temp[18] << 8 ) | temp[19] ); // mag z-axis is reversed

    acc_xyz[0] -= mpu9250_acc_offset[0];
    acc_xyz[1] -= mpu9250_acc_offset[1];
    acc_xyz[2] -= mpu9250_acc_offset[2];
    gyro_xyz[0] -= mpu9250_gyro_offset[0];
    gyro_xyz[1] -= mpu9250_gyro_offset[1];
    gyro_xyz[2] -= mpu9250_gyro_offset[2];
    mag_xyz[0] = ( ( int32_t ) ( mag_xyz[0] - mpu9250_mag_offset[0] ) * mpu9250_mag_sensitivity[0] ) >> 5;
    mag_xyz[1] = ( ( int32_t ) ( mag_xyz[1] - mpu9250_mag_offset[1] ) * mpu9250_mag_sensitivity[1] ) >> 5;
    mag_xyz[2] = ( ( int32_t ) ( mag_xyz[2] - mpu9250_mag_offset[2] ) * mpu9250_mag_sensitivity[2] ) >> 5;
    return result;
}
