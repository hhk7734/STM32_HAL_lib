/**
 * filename : mpu9250.h
 *
 * created  : 2018/09/09
 *
 * Hyeon-ki, Hong
 * hhk7734@gmail.com
 *
 * purpose : MPU-9250 ; accelerometer, gyroscope, magnetometer
 */

#ifndef _MPU9250_H_
#define _MPU9250_H_
#ifdef __cplusplus
extern "C"
{
#endif

#define MPU9250_DLPF_CFG_0          0x00    // Bandwidth 250 Hz Delay 0.97 ms Fs 8 kHz
#define MPU9250_DLPF_CFG_1          0x01
#define MPU9250_DLPF_CFG_2          0x02
#define MPU9250_DLPF_CFG_3          0x03
#define MPU9250_DLPF_CFG_4          0x04
#define MPU9250_DLPF_CFG_5          0x05
#define MPU9250_DLPF_CFG_6          0x06
#define MPU9250_DLPF_CFG_7          0x07
#define MPU9250_GYRO_FS_SEL_250DPS  0x00
#define MPU9250_GYRO_FS_SEL_500DPS  0x08
#define MPU9250_GYRO_FS_SEL_1000DPS 0x10
#define MPU9250_GYRO_FS_SEL_2000DPS 0x18
#define MPU9250_GYRO_FCHOICE_X0     0x00
#define MPU9250_GYRO_FCHOICE_01     0x01
#define MPU9250_GYRO_FCHOICE_11     0x03

#define MPU9250_ACCEL_FS_SEL_2G     0x00
#define MPU9250_ACCEL_FS_SEL_4G     0x08
#define MPU9250_ACCEL_FS_SEL_8G     0x10
#define MPU9250_ACCEL_FS_SEL_16G    0x18
#define MPU9250_ACCEL_FCHOICE_0     0x00
#define MPU9250_ACCEL_FCHOICE_1     0x08
#define MPU9250_A_DLPFCFG_0         0x00
#define MPU9250_A_DLPFCFG_1         0x01
#define MPU9250_A_DLPFCFG_2         0x02
#define MPU9250_A_DLPFCFG_3         0x03    // Bandwidth 44.8 Hz Delay 4.88 ms Fs 1 kHz
#define MPU9250_A_DLPFCFG_4         0x04
#define MPU9250_A_DLPFCFG_5         0x05
#define MPU9250_A_DLPFCFG_6         0x06
#define MPU9250_A_DLPFCFG_7         0x07

/**
 * include header for stm32 mcu
 */
#include "stm32f1xx_hal.h"

/**
 * USER SETTING
 */
#define MPU9250_AD0 0
#define MPU9250_GYRO_FS_SEL     MPU9250_GYRO_FS_SEL_2000DPS
#define MPU9250_GYRO_FCHOICE    MPU9250_GYRO_FCHOICE_11
#define MPU9250_DLPF_CFG        MPU9250_DLPF_CFG_0
#define MPU9250_ACCEL_FS_SEL    MPU9250_ACCEL_FS_SEL_8G
#define MPU9250_ACCEL_FCHOICE   MPU9250_ACCEL_FCHOICE_1
#define MPU9250_A_DLPFCFG       MPU9250_A_DLPFCFG_3

#if MPU9250_ACCEL_FS_SEL == MPU9250_ACCEL_FS_SEL_2G
#define MPU9250_ACCEL_1G_SCALE 16384
#elif MPU9250_ACCEL_FS_SEL == MPU9250_ACCEL_FS_SEL_4G
#define MPU9250_ACCEL_1G_SCALE 8192
#elif MPU9250_ACCEL_FS_SEL == MPU9250_ACCEL_FS_SEL_8G
#define MPU9250_ACCEL_1G_SCALE 4096
#elif MPU9250_ACCEL_FS_SEL == MPU9250_ACCEL_FS_SEL_16G
#define MPU9250_ACCEL_1G_SCALE 2048
#endif

#if MPU9250_GYRO_FS_SEL == MPU9250_GYRO_FS_SEL_250DPS
#define MPU9250_GYRO_1DPS_SCALE 131
#elif MPU9250_GYRO_FS_SEL == MPU9250_GYRO_FS_SEL_500DPS
#define MPU9250_GYRO_1DPS_SCALE 65.5f
#elif MPU9250_GYRO_FS_SEL == MPU9250_GYRO_FS_SEL_1000DPS
#define MPU9250_GYRO_1DPS_SCALE 32.8f
#elif MPU9250_GYRO_FS_SEL == MPU9250_GYRO_FS_SEL_2000DPS
#define MPU9250_GYRO_1DPS_SCALE 16.4f
#endif

extern int16_t mpu9250_acc_offset[3];
extern int16_t mpu9250_gyro_offset[3];
extern int16_t mpu9250_mag_offset[3];
extern int16_t mpu9250_mag_sensitivity[3];

HAL_StatusTypeDef mpu9250_write_reg( uint8_t reg , uint8_t data );
HAL_StatusTypeDef mpu9250_read_reg( uint8_t reg , uint8_t *data , uint8_t size );
HAL_StatusTypeDef mpu9250_setup( I2C_HandleTypeDef *i2c );

HAL_StatusTypeDef mpu9250_acc_calibration( void );
HAL_StatusTypeDef mpu9250_gyro_calibration( void );
HAL_StatusTypeDef mpu9250_mag_calibration( void );

HAL_StatusTypeDef mpu9250_get_acc( int16_t *xyz );
HAL_StatusTypeDef mpu9250_get_gyro( int16_t *xyz );
HAL_StatusTypeDef mpu9250_get_mag( int16_t *xyz );
HAL_StatusTypeDef mpu9250_get_all( int16_t *acc_xyz , int16_t *gyro_xyz , int16_t *mag_xyz );

#ifdef __cplusplus
}
#endif
#endif /* _MPU9250_H_ */
