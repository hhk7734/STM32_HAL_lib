/**
 * filename : uart.c
 *
 * created  : 2018/08/26
 * edited   : 2018/09/02
 *
 * Hyeon-ki, Hong
 * hhk7734@gmail.com
 *
 * purpose : UART communication
 */

#include "uart.h"

#ifdef _UART1_
volatile uint8_t uart1_rx_data = 0;
volatile uint8_t uart1_rx_buf_head = 0;
volatile uint8_t uart1_rx_buf_tail = 0;
volatile uint8_t uart1_rx_buf[UART1_RX_BUF_SIZE];
volatile uint8_t uart1_tx_buf_head = 0;
volatile uint8_t uart1_tx_buf_tail = 0;
volatile uint8_t uart1_tx_buf[UART1_TX_BUF_SIZE];
#endif

#ifdef _UART2_
volatile uint8_t uart2_rx_data = 0;
volatile uint8_t uart2_rx_buf_head = 0;
volatile uint8_t uart2_rx_buf_tail = 0;
volatile uint8_t uart2_rx_buf[UART2_RX_BUF_SIZE];
volatile uint8_t uart2_tx_buf_head = 0;
volatile uint8_t uart2_tx_buf_tail = 0;
volatile uint8_t uart2_tx_buf[UART2_TX_BUF_SIZE];
#endif

#if defined(_UART1_) || defined(_UART2_)
void HAL_UART_RxCpltCallback( UART_HandleTypeDef *huart )
{
#ifdef _UART1_
    if ( huart->Instance == USART1 )
    {
        uart1_rx_buf[uart1_rx_buf_head] = uart1_rx_data;
        uart1_rx_buf_head = ( uart1_rx_buf_head + 1 ) % UART1_RX_BUF_SIZE;
        if ( uart1_rx_buf_head == uart1_rx_buf_tail )
            uart1_rx_buf_tail = ( uart1_rx_buf_tail + 1 ) % UART1_RX_BUF_SIZE;
        HAL_UART_Receive_IT( & huart1 , & uart1_rx_data , 1 );
    }
#endif
#ifdef _UART2_
    if ( huart->Instance == USART2 )
    {
        uart2_rx_buf[uart2_rx_buf_head] = uart2_rx_data;
        uart2_rx_buf_head = ( uart2_rx_buf_head + 1 ) % UART2_RX_BUF_SIZE;
        if ( uart2_rx_buf_head == uart2_rx_buf_tail )
        uart2_rx_buf_tail = ( uart2_rx_buf_tail + 1 ) % UART2_RX_BUF_SIZE;
        HAL_UART_Receive_IT( & huart2 , & uart2_rx_data , 1 );
    }
#endif
}

void HAL_UART_TxCpltCallback( UART_HandleTypeDef *huart )
{
#ifdef _UART1_
    if ( huart->Instance == USART1 )
    {
        if ( uart1_tx_buf_head != uart1_tx_buf_tail )
        {
            HAL_UART_Transmit_IT( & huart1 , & uart1_tx_buf[uart1_tx_buf_tail] , 1 );
            uart1_tx_buf_tail = ( uart1_tx_buf_tail + 1 ) % UART1_TX_BUF_SIZE;
        }
    }
#endif
#ifdef _UART2_
    if ( huart->Instance == USART2 )
    {
        if ( uart2_tx_buf_head == uart2_tx_buf_tail )
        {
            HAL_UART_Transmit_IT( & huart2 , & uart2_tx_buf[uart2_tx_buf_tail] , 1 );
            uart2_tx_buf_tail = ( uart2_tx_buf_tail + 1 ) % UART2_TX_BUF_SIZE;
        }
    }
#endif
}

void uart_begin( void )
{
#ifdef _UART1_
    HAL_UART_Receive_IT( & huart1 , & uart1_rx_data , 1 );
#endif
#ifdef _UART2_
    HAL_UART_Receive_IT( & huart2 , & uart2_rx_data , 1 );
#endif
}
#endif // _UART1_ || _UART2_

#ifdef _UART1_
uint8_t uart1_read( void )
{
    if ( uart1_rx_buf_head == uart1_rx_buf_tail )
        return 0;
    else
    {
        uint8_t buf = uart1_rx_buf[uart1_rx_buf_tail];
        uart1_rx_buf_tail = ( uart1_rx_buf_tail + 1 ) % UART1_RX_BUF_SIZE;
        return buf;
    }
}

void uart1_write( uint8_t data )
{
    if ( HAL_UART_Transmit_IT( & huart1 , & data , 1 ) != HAL_OK )
    {
        uint8_t temp = ( uart1_tx_buf_head + 1 ) % UART1_TX_BUF_SIZE;
        while ( temp == uart1_tx_buf_tail )
            ;
        uart1_tx_buf[uart1_tx_buf_head] = data;
        uart1_tx_buf_head = temp;
    }
}

void uart1_print_str( char* str )
{
    while ( * str )
    {
        uart1_write( * str++ );
    }
}

uint8_t uart1_available( void )
{
    return ( UART1_RX_BUF_SIZE + uart1_rx_buf_head - uart1_rx_buf_tail ) % UART1_RX_BUF_SIZE;
}

void uart1_flush( void )
{
    uart1_rx_buf_tail = uart1_rx_buf_head;
}
#endif /* _UART1_ */

#ifdef _UART2_
uint8_t uart2_read( void )
{
    if ( uart2_rx_buf_head == uart2_rx_buf_tail )
    return 0;
    else
    {
        uint8_t buf = uart2_rx_buf[uart2_rx_buf_tail];
        uart2_rx_buf_tail = ( uart2_rx_buf_tail + 1 ) % UART2_RX_BUF_SIZE;
        return buf;
    }
}

void uart2_write( uint8_t data )
{
    if ( HAL_UART_Transmit_IT( & huart2 , & data , 1 ) != HAL_OK )
    {
        uint8_t temp = ( uart2_tx_buf_head + 1 ) % UART2_TX_BUF_SIZE;
        while ( temp == uart2_tx_buf_tail )
        ;
        uart2_tx_buf[uart2_tx_buf_head] = data;
        uart2_tx_buf_head = temp;
    }
}

void uart2_print_str( char* str )
{
    while ( * str )
    {
        uart2_write( * str++ );
    }
}

uint8_t uart2_available( void )
{
    return ( UART2_RX_BUF_SIZE + uart2_rx_buf_head - uart2_rx_buf_tail ) % UART2_RX_BUF_SIZE;
}

void uart2_flush( void )
{
    uart2_rx_buf_tail = uart2_rx_buf_head;
}
#endif /* _UART2_ */

#if defined(UART_PRINTF) || defined(ITM_PRINTF)
int _write( int32_t file , uint8_t *ptr , int32_t len )
{
    /* Implement your write code here, this is used by puts and printf for example */

    for ( int16_t i = 0 ; i < len ; ++i )
    {
        uint8_t temp = * ptr++;
#ifdef UART_PRINTF
        UART_PRINTF( temp );
#endif
#ifdef ITM_PRINTF
        ITM_PRINTF( temp );
#endif
    }
    return len;
}
#endif