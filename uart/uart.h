/**
 * filename : uart.h
 *
 * created  : 2018/08/26
 * edited   : 2018/09/02
 *
 * Hyeon-ki, Hong
 * hhk7734@gmail.com
 *
 * purpose : UART communication
 */

#ifndef _UART_H_
#define _UART_H_
#ifdef __cplusplus
extern "C"
{
#endif

/**
 * include header for stm32 mcu
 */
#include "stm32f1xx_hal.h"

/**
 * select UART and set printf
 */
// #define _UART1_
//#define _UART2_
// #define UART_PRINTF(x) uart1_write(x)
#define ITM_PRINTF(x) ITM_SendChar(x)

#ifdef _UART1_
#define UART1_RX_BUF_SIZE 64 //must be power of 2
#define UART1_TX_BUF_SIZE 64
extern UART_HandleTypeDef huart1;
#endif

#ifdef _UART2_
#define UART2_RX_BUF_SIZE 64 //must be power of 2
#define UART2_TX_BUF_SIZE 64
extern UART_HandleTypeDef huart2;
#endif

#if defined(_UART1_) || defined(_UART2_)
void uart_begin( void );
#endif

#ifdef _UART1_
uint8_t uart1_read( void );
void    uart1_write( uint8_t data );
void    uart1_print_str( char* str );
uint8_t uart1_available( void );
void    uart1_flush( void );
#endif

#ifdef _UART2_
uint8_t uart2_read( void );
void    uart2_write( uint8_t data );
void    uart2_print_str( char* str );
uint8_t uart2_available( void );
void    uart2_flush( void );
#endif

#ifdef __cplusplus
}
#endif

#endif /* _UART_H_ */
